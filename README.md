# epdnorway

Python module (pypi.org) to consume XML data from https://digi.epd-norge.no/ into any
python project.

## Requirements

python v3.7 xmltodict (xmltodict-0.12.0)

## Installation

```cmd
git clone https://samuelduchesne@bitbucket.org/samuelduchesne/epdnorway.git
cd epdnorway
pip install -e .
```

## CLI Usage

Example usage:

To get the help

```cmd
epnorway --help
```

Dump the first 10 epds to a json file

```cmd
epdnorway list-epds --pageSize 10 > test.json
```

Generate an excel report for all epds:

```cmd
epdnorway generate-excel --pageSize 10 --dst "first_10_epds.xlsx"
```


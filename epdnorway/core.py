import json
import logging
import re

import pandas as pd
import requests
import xmltodict

BASE_URI = "https://epdnorway.lca-data.com/resource"
DATASTOCK = "91413340-7bf0-4f88-a952-0f91cba685df"

LIFE_CYCLE_METHODS = {
    "AP": {
        "id": "b4274add-93b7-4905-a5e4-2e878c4e4216",
        "desc": "Acidification potential",
    },
    "ADPF": {
        "id": "804ebcdf-309d-4098-8ed8-fdaf2f389981",
        "desc": "Abiotic depletion potential for fossil resources",
    },
    "ADPE": {
        "id": "f7c73bb9-ab1a-4249-9c6d-379a0de6f67e",
        "desc": "Abiotic depletion potential for non fossil resources",
    },
    "EP": {
        "id": "f58827d0-b407-4ec6-be75-8b69efb98a0f",
        "desc": "Eutrophication potential",
    },
    "GWP": {
        "id": "77e416eb-a363-4258-a04e-171d843a6460",
        "desc": "Global warming potential",
    },
    "ODP": {
        "id": "06dcd26f-025f-401a-a7c1-5e457eb54637",
        "desc": "Ozone Depletion Potential",
    },
    "POCP": {
        "id": "1e84a202-dae6-42aa-9e9d-71ea48b8be00",
        "desc": "Photochemical Ozone Creation Potential",
    },
}

EXCHANGE_FLOWS = {
    "CRU": {
        "id": "4f69ed08-b1f3-4df5-acc5-4f659a288d37",
        "desc": "Components for re-use",
    },
    "EEE": {
        "id": "4da0c987-2b76-40d6-9e9e-82a017aaaf29",
        "desc": "Exported electrical energy",
    },
    "EET": {
        "id": "98daf38a-7a79-46d3-9a37-2b7bd0955810",
        "desc": "Exported thermal energy (EET)",
    },
    "HWD": {
        "id": "430f9e0f-59b2-46a0-8e0d-55e0e84948fc",
        "desc": "Hazardous waste disposed",
    },
    "MER": {
        "id": "59a9181c-3aaf-46ee-8b13-2b3723b6e447",
        "desc": "Materials for energy recovery",
    },
    "MFR": {
        "id": "d7fe48a5-4103-49c8-9aae-b0b5dfdbd6ae",
        "desc": "Materials for recycling",
    },
    "FW": {
        "id": "3cf952c8-f3a4-461d-8c96-96456ca62246",
        "desc": "Use of net fresh water",
    },
    "NHWD": {
        "id": "b29ef66b-e286-4afa-949f-62f1a7b4d7fa",
        "desc": "Non hazardous waste dispose",
    },
    "NRSF": {
        "id": "89def144-d39a-4287-b86f-efde453ddcb2",
        "desc": "Use of non renewable secondary fuels",
    },
    "PENRE": {
        "id": "ac857178-2b45-46ec-892a-a9a4332f0372",
        "desc": "Use of non renewable primary energy",
    },
    "PENRM": {
        "id": "1421caa0-679d-4bf4-b282-0eb850ccae27",
        "desc": "Use of non renewable primary energy resources used as raw materials",
    },
    "PENRT": {
        "id": "06159210-646b-4c8d-8583-da9b3b95a6c1",
        "desc": "Total use of non renewable primary energy resource",
    },
    "PERE": {
        "id": "20f32be5-0398-4288-9b6d-accddd195317",
        "desc": "Use of renewable primary energy",
    },
    "PERM": {
        "id": "fb3ec0de-548d-4508-aea5-00b73bf6f702",
        "desc": "Use of renewable primary energy resources used as raw materials",
    },
    "PERT": {
        "id": "53f97275-fa8a-4cdd-9024-65936002acd0",
        "desc": "Total use of renewable primary energy resources",
    },
    "RSF": {
        "id": "64333088-a55f-4aa2-9a31-c10b07816787",
        "desc": "Use of renewable secondary fuels",
    },
    "RWD": {
        "id": "3449546e-52ad-4b39-b809-9fb77cea8ff6",
        "desc": "Radioactive waste disposed",
    },
    "SM": {
        "id": "c6a1f35f-2d09-4f54-8dfb-97e502e1ce92",
        "desc": "Use of secondary material",
    },
}

KNOWN_MODULES = [
    "A1",
    "A2",
    "A3",
    "A1-A3",
    "A4",
    "A5",
    "B1",
    "B2",
    "B3",
    "B4",
    "B5",
    "B6",
    "B7",
    "C1",
    "C2",
    "C3",
    "C4",
    "D",
]


def list_all(
    pageSize=1000,
    format="json",
    search=True,
    startIndex=0,
    sortOrder=True,
    sortBy="name",
):
    """Returns all EPDs. Optionally set a certain number (Page length).

    Args:
        pageSize (int): How many epds to return. Defaults to all

    Returns:
        list: List of EPDs
    """
    payload = {
        "format": format,
        "search": search,
        "startIndex": startIndex,
        "pageSize": pageSize,
        "sortOrder": sortOrder,
        "sortBy": sortBy,
    }
    with requests.get(
        "{}/datastocks/{}/processes".format(BASE_URI, DATASTOCK), params=payload
    ) as url:
        data = json.loads(url.text)
        return data["data"]


def list_query(
    q,
    pageSize=1000,
    format="json",
    search=True,
    startIndex=0,
    sortOrder=True,
    sortBy="name",
):
    payload = {
        "format": format,
        "search": search,
        "startIndex": startIndex,
        "pageSize": pageSize,
        "sortOrder": sortOrder,
        "sortBy": sortBy,
    }
    with requests.get(
        "{}/datastocks/{}/processes".format(BASE_URI, DATASTOCK), params=payload
    ) as url:
        data = json.loads(url.text)
        return [
            item
            for item in data["data"]
            if re.search(q, item["name"] + item.get("classific", ""), re.IGNORECASE)
        ]


def fetch(uuid, format="xml", lang="en"):
    with requests.get(
        "{}/datastocks/{}/processes/{}".format(BASE_URI, DATASTOCK, uuid),
        params=dict(format=format, lang=lang),
    ) as url:
        return xmltodict.parse(
            url.text,
            process_namespaces=True,
            namespaces={
                "http://lca.jrc.it/ILCD/Process": None,
                "http://lca.jrc.it/ILCD/Common": "common",
                "http://www.w3.org/XML/1998/namespace": "xml",
                "http://www.iai.kit.edu/EPD/2013": "epd",
            },
        )


def ensure_list(obj):
    if isinstance(obj, list):
        return obj
    else:
        return [obj]


def short_desc(elem):
    return elem["common:shortDescription"]


def filterShortDescByLang(arr, lang):
    el = [el for el in arr if el["common:shortDescription"]["@xml:lang"] == lang]
    return el


def filterByLang(arr, lang):
    el = [el for el in arr if el["@xml:lang"] == lang]
    if not el:
        el = [arr[0]]
    return el


def filterByProp(arr, prop):
    el = [el for el in arr if el["@id"] == prop]
    return el


""" Are we sure we want to fallback to 0.0 ? """


def forceFloat(val):
    return float(0.0 if val is None else val)


class DataSet:
    """  exchanges """

    def __getExchangeFlows(self):
        """Gets the different exchange flows and the ReferenceFlow of the EPD"""
        exchange = self.raw
        refFlowId = exchange["processDataSet"]["processInformation"][
            "quantitativeReference"
        ]["referenceToReferenceFlow"]
        flows = {}
        exchanges = exchange["processDataSet"]["exchanges"]["exchange"]
        for ex in exchanges:
            for flow in EXCHANGE_FLOWS:
                if (
                    ex["referenceToFlowDataSet"]["@refObjectId"]
                    == EXCHANGE_FLOWS[flow]["id"]
                ):
                    amounts = {}
                    for mod in KNOWN_MODULES:
                        for y in [
                            y
                            for y in ensure_list(
                                ex.get("common:other", {}).get("epd:amount", [])
                            )
                            if y.get("@epd:module", "") == mod
                        ]:
                            try:
                                amounts[mod] = float(y.get("#text", 0))
                            except ValueError:
                                # Somehow, "#text" is a string
                                amounts[mod] = y.get("#text", "")
                    flows[flow] = ExchangeFlow(
                        {
                            "id": EXCHANGE_FLOWS[flow]["id"],
                            "name": EXCHANGE_FLOWS[flow]["desc"],
                            "meanAmount": ex["meanAmount"],
                            "direction": ex["exchangeDirection"],
                            "unit": ex["common:other"][
                                "epd:referenceToUnitGroupDataSet"
                            ]["common:shortDescription"],
                            "amounts": amounts,
                            "acronyme": flow,
                        }
                    )
            if ex["@dataSetInternalID"] == refFlowId:
                self.referenceFlow = ReferenceFlow.from_xmldict(ex, lang=self.lang)

        return flows

    @property
    def refflow_material_properties(self):
        """

        Returns:
            list: List of material properties of the reference flow of this epd.
        """
        return self.referenceFlow.material_properties

    def __getImpactAssessments(self):
        """lcia"""
        lcia = self.raw["processDataSet"]["LCIAResults"]["LCIAResult"]
        imps = {}
        for l in lcia:
            for lcm in LIFE_CYCLE_METHODS:
                if (
                    l["referenceToLCIAMethodDataSet"]["@refObjectId"]
                    == LIFE_CYCLE_METHODS[lcm]["id"]
                ):

                    amounts = {}
                    for mod in KNOWN_MODULES:
                        for y in [
                            y
                            for y in ensure_list(l["common:other"]["epd:amount"])
                            if y.get("@epd:module", "") == mod
                        ]:
                            try:
                                amounts[mod] = float(y.get("#text", 0))
                            except ValueError:
                                # Somehow, "#text" is a string
                                amounts[mod] = y.get("#text", "")
                    imps[lcm] = ImpactAssessment(
                        {
                            "id": LIFE_CYCLE_METHODS[lcm]["id"],
                            "name": LIFE_CYCLE_METHODS[lcm]["desc"],
                            "meanAmount": l["meanAmount"],
                            "unit": l["common:other"][
                                "epd:referenceToUnitGroupDataSet"
                            ]["common:shortDescription"],
                            "amounts": amounts,
                        }
                    )
        return imps

    def __extractLifeCycle(self, exchange):
        r = {
            "direction": exchange["exchangeDirection"],
            "unit": exchange["common:other"]["epd:referenceToUnitGroupDataSet"][
                "common:shortDescription"
            ],
        }

        for x in [
            x
            for x in ensure_list(
                exchange["referenceToFlowDataSet"]["common:shortDescription"]
            )
            if x["@xml:lang"] == "en"
        ]:
            r["indicator"] = x.get("#text")

        for mod in KNOWN_MODULES:
            for x in [
                x
                for x in ensure_list(exchange["common:other"]["epd:amount"])
                if x.get("@epd:module", "") == mod
            ]:
                r[mod] = float(
                    x.get("#text", 0)
                )  # TODO missing values = 0, or undef key?

        return r

    def __extractImpactAssessment(self, result):
        """ lcia """
        r = {
            "unit": result["common:other"]["epd:referenceToUnitGroupDataSet"][
                "common:shortDescription"
            ]
        }

        for x in [
            x
            for x in ensure_list(
                result["referenceToLCIAMethodDataSet"]["common:shortDescription"]
            )
            if x["@xml:lang"] == "en"
        ]:
            r["indicator"] = x.get("#text")

        for mod in KNOWN_MODULES:
            for x in [
                x
                for x in ensure_list(result["common:other"]["epd:amount"])
                if x["@epd:module"] == mod
            ]:
                r[mod] = float(x.get("#text", 0))

        return r

    def availableFlows(self):
        flows = []
        for fl in self.exchangeFlows.keys():
            flows.append({fl: EXCHANGE_FLOWS[fl]["desc"]})
        return flows

    def availableImpactAssessments(self):
        lcias = []
        for lc in self.impactAssessments.keys():
            lcias.append({lc: LIFE_CYCLE_METHODS[lc]["desc"]})
        return lcias

    def getFlow(self, flow):
        fl = self.exchangeFlows[flow]
        if fl:
            # fl.ref = fl.fetchRef()
            return fl

    def getImpact(self, lcia):
        ia = self.impactAssessments[lcia]
        if ia:
            ia.ref = ia.fetchRef()
            return ia

    def __init__(self, uuid, lang="no"):
        self.lang = lang
        raw = fetch(uuid, lang=lang)
        info = raw["processDataSet"]["processInformation"]["dataSetInformation"]
        name = filterByLang(ensure_list(info["name"]["baseName"]), lang)
        desc = filterByLang(ensure_list(info["common:generalComment"]), lang)
        self.raw = raw  # TODO: remove when no longer needed
        self.id = uuid
        self.name = name[0].get("#text")
        self.description = desc[0].get("#text")
        self.tags = self.get_tags()
        self.exchangeFlows = self.__getExchangeFlows()
        self.impactAssessments = self.__getImpactAssessments()

    def get_tags(self):
        """Tag logic"""
        info = self.raw["processDataSet"]["processInformation"]["dataSetInformation"][
            "classificationInformation"
        ]
        tags = None
        if info:
            try:
                tags = [
                    c["#text"]
                    for c in ensure_list(info["common:classification"]["common:class"])
                ]
            except Exception as e:
                logging.warning(f"Could not process tags for epd {self.id}")
                tags = []
        return tags

    def dump_json(self):
        return json.dumps(self.raw)


class MaterialProperty(object):
    def __init__(self, name, desc, unit, value):
        self.name = name
        self.desc = desc
        self.unit = unit
        self.value = value

    def __str__(self):
        return self.name + ": " + self.value + " " + self.unit

    def __repr__(self):
        return self.__str__()


class ProductFlowDataSet:
    """Main Class for ReferenceFlows and ExchangeFlows"""

    def __init__(self, id, lang="no", format="json"):

        with requests.get(
            "{}/flows/{}".format(BASE_URI, id), params=dict(format=format)
        ) as url:
            data = xmltodict.parse(
                url.text,
                process_namespaces=True,
                namespaces={
                    "http://lca.jrc.it/ILCD/Flow": "f",
                    "http://www.w3.org/XML/1998/namespace": "xml",
                    "http://lca.jrc.it/ILCD/Common": "common",
                    "http://www.matml.org/": "mat",
                },
            )
        info = data["f:flowDataSet"]["f:flowInformation"]["f:dataSetInformation"]
        name = filterByLang(ensure_list(info["f:name"]["f:baseName"]), lang)
        self.lang = lang
        self.data = data
        self.id = info["common:UUID"]
        self.name = name[0].get("#text")
        self.material_properties = self.__fetchMaterialProperties(info, lang)
        info = self.data["f:flowDataSet"]["f:flowProperties"]
        self.reference_flow_property = self.__fetchQuantitativeRef(info, lang)

    def __fetchMaterialProperties(self, info, lang="no"):
        if not info.get("common:other"):
            return None
        elif info["common:other"] and info.get("common:other", {}).get("mat:MatML_Doc"):
            matml = info["common:other"]["mat:MatML_Doc"]
            matprop_list = []
            for x in ensure_list(
                matml["mat:Material"]["mat:BulkDetails"]["mat:PropertyData"]
            ):
                prop = x["@property"]
                v = x["mat:Data"]["#text"]
                u = filterByProp(
                    ensure_list(matml["mat:Metadata"]["mat:PropertyDetails"]), prop
                )
                mp = MaterialProperty(
                    name=u[0]["mat:Name"],
                    desc=u[0]["mat:Units"]["@description"],
                    unit=u[0]["mat:Units"]["@name"],
                    value=x["mat:Data"]["#text"],
                )
                matprop_list.append(mp)
            return matprop_list
        else:
            logging.warning(
                f"Could not fetch/parse material properties of product "
                f"flow data set {self.id}"
            )
            return None

    def __fetchQuantitativeRef(self, info, lang="no"):
        set_ = info["f:flowProperty"]["f:referenceToFlowPropertyDataSet"]

        def valid_uuid(uuid):
            """Will grab the uuid part in the url"""
            parts = uuid.split("/")
            return next(filter(lambda x: "xml" in x, parts)).split(".")[0]

        if "@uri" in set_.keys():
            url = set_["@uri"]
            query = requests.utils.urlparse(url).query
            if query != "":
                params = dict(x.split("=") for x in query.split("&"))
                uuid = params["uuid"]
            else:
                uuid = valid_uuid(url)
        elif "@refObjectId" in set_.keys():
            uuid = set_["@refObjectId"]

        return self.__fetchRef(uuid, lang)

    def __fetchRef(self, uuid, lang="no", format="xml"):
        with requests.get(
            "{}/flowproperties/{}".format(BASE_URI, uuid), params=dict(format=format)
        ) as url:
            if url.ok:
                ref = xmltodict.parse(url.text)
                if ref:
                    return FlowPropertyDataSet(ref, lang=lang)
            else:
                raise requests.exceptions.RequestException(url.text)


class ExchangeFlow:
    def __init__(self, data):
        self.data = data
        self.id = data.get("id")
        self.name = data.get("name")
        self.meanAmount = data.get("meanAmount")
        self.direction = data.get("direction")
        self.unit = data.get("unit")
        self.amounts = data.get("amounts")
        self.acronyme = data.get("acronyme")


class ReferenceFlow(ProductFlowDataSet):
    def __init__(self, data, lang="no", format="json"):
        super().__init__(data.get("id"), lang, format=format)

        self.meanAmount = data.get("meanAmount")
        self.resultingAmount = data.get("resultingAmount")
        self.version = data.get("version")

    def __repr__(self):
        return (
            self.name
            + " - "
            + self.meanAmount
            + " * "
            + self.resultingAmount
            + f" {self.reference_flow_property.default_unit['u:name']} "
            + f" ({self.reference_flow_property.name})"
        )

    def __str__(self):
        return self.__repr__()

    @classmethod
    def from_xmldict(cls, ex, lang):
        """Pass xml dict to constructor"""
        data = dict(
            id=ex["referenceToFlowDataSet"]["@refObjectId"],
            name=ReferenceFlow.get_name(ex, lang),
            meanAmount=ex["meanAmount"],
            resultingAmount=ex["resultingAmount"],
            version=ex["referenceToFlowDataSet"]["@version"],
        )
        return cls(data, lang=lang)

    @classmethod
    def get_name(cls, ex, lang):
        """Gets product name based on lang (if needed)"""
        description_ = ex["referenceToFlowDataSet"]["common:shortDescription"]
        if isinstance(description_, list):
            return filterByLang(description_, lang=lang)[0]["#text"]
        else:
            return description_["#text"]


class ImpactAssessment:
    def __init__(self, data):
        """Fetch then referenced life cycle dataset."""
        self.data = data
        self.id = data.get("id")
        self.name = data.get("name")
        self.meanAmount = data.get("meanAmount")
        self.unit = data.get("unit")
        self.amounts = data.get("amounts")

    def fetchRef(self, format="xml"):
        with requests.get(
            "{}/lciamethods/{}".format(BASE_URI, self.data.get("id")),
            params=dict(format=format),
        ) as url:
            return xmltodict.parse(url.text)


class UnitGroupDataSet:
    def __init__(self, data):
        self.data = data
        self.internal_id = data["u:unitGroupDataSet"]["u:unitGroupInformation"][
            "u:quantitativeReference"
        ]["u:referenceToReferenceUnit"]
        self.all_units = data["u:unitGroupDataSet"]["u:units"]["u:unit"]

    def default_value(self):
        return self.all_units[int(self.internal_id)]


def parse_tag(tag):
    """Remove the prefix in an xml tag."""
    if ":" in tag:
        prefix, tag = tag.split(":")
    return tag


class FlowPropertyDataSet:
    def __init__(self, data, lang="no"):
        info = data["fp:flowPropertyDataSet"]["fp:flowPropertiesInformation"][
            "fp:dataSetInformation"
        ]
        name = filterByLang(ensure_list(info["common:name"]), lang)
        self.data = data
        self.id = info["common:UUID"]
        quantitative_ref = data["fp:flowPropertyDataSet"][
            "fp:flowPropertiesInformation"
        ]["fp:quantitativeReference"]["fp:referenceToReferenceUnitGroup"]
        self.quantitative_refId = quantitative_ref["@refObjectId"]
        self.name = name[0].get("#text")
        self.default_unit = self.__fetchUnits().default_value()

    def __fetchUnits(self, format="xml"):
        with requests.get(
            "{}/unitgroups/{}".format(BASE_URI, self.quantitative_refId),
            params=dict(format=format),
        ) as url:
            ref = xmltodict.parse(
                url.text,
                process_namespaces=True,
                namespaces={
                    "http://lca.jrc.it/ILCD/Common": "common",
                    "http://lca.jrc.it/ILCD/UnitGroup": "u",
                },
            )
            if ref:
                return UnitGroupDataSet(ref)

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return self.name + f" ({self.default_unit.get('u:name')})"


def strip_namespace(xml):
    import xml.etree.ElementTree as ET

    # instead of ET.fromstring(xml)
    it = ET.iterparse(xml)
    for _, el in it:
        prefix, has_namespace, postfix = el.tag.partition("}")
        if has_namespace:
            el.tag = postfix  # strip all namespaces
    return it


def safe_getFlow(dataSet, flow):
    """A wraper around epdnorway.DataSet.getFlow to safely apply to a DataFrame or a Series."""
    try:
        return dataSet.getFlow(flow).data
        # .data is applied to return the dict instead of the object
    except:
        pass


def get_allflow_amounts(dataset):
    """Returns the amounts of avery possible flow for a particular DataSet object"""
    try:
        all_flow_amounts = {}
        for flowdict in dataset.availableFlows():
            (flow,) = tuple(flowdict)  # unpack the key (of the dict) into a tuple
            flowdata = safe_getFlow(dataset, flow)
            all_flow_amounts[flow] = pd.Series(get_amounts(flowdata))
        return pd.DataFrame(all_flow_amounts).unstack()
    except Exception as e:
        return pd.Series()


def safe_getLcia(dataSet, lcia):
    """A wraper around epdnorway.DataSet.getFlow to safely apply to a DataFrame or a Series."""
    try:
        return dataSet.getImpact(lcia).data
        # .data is applied to return the dict instead of the object
    except:
        pass


def get_amounts(flow):
    try:
        return flow["amounts"]
    except:
        pass


def get_alllcia(dataset):
    """Returns the amounts of avery possible flow for a particular DataSet object"""
    try:
        all_lcai_data = {}
        for lciadict in dataset.availableImpactAssessments():
            (lcia,) = tuple(lciadict)  # unpack the key (of the dict) into a tuple
            flowdata = safe_getLcia(dataset, lcia)
            all_lcai_data[lcia] = pd.Series(get_amounts(flowdata))
        return pd.DataFrame(all_lcai_data).unstack()
    except Exception as e:
        return pd.Series()


def safe_dataset(uuid, lang):
    """A wraper around epdnorway.DataSet to safely apply to a DataFrame"""
    try:
        return DataSet(uuid, lang)
    except Exception as e:
        return e


def safe_refFlow(dataset):
    """Gets the Reference Flow Data for an EPD"""
    try:
        ref = dataset.referenceFlow
        return dict(
            name=ref.name,
            meanAmount=ref.meanAmount,
            resultingAmount=ref.resultingAmount,
            defaultUnit=ref.reference_flow_property.default_unit["u:name"],
            unitName=ref.reference_flow_property.name,
            material_properties=[str(a) for a in (ref.material_properties or [])],
        )
    except Exception as e:
        return e

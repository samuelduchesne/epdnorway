import json

import click
import pandas as pd
from tqdm import tqdm

from epdnorway.core import (
    get_allflow_amounts,
    get_alllcia,
    list_all,
    safe_dataset,
    safe_refFlow,
)


@click.group()
def cli():
    """EpdNorway command line interface."""
    pass


@cli.command()
@click.option(
    "--pageSize", default=None, required=False, help="How many epds to return."
)
@click.option("--format", default="json")
@click.option("--search", default=True)
@click.option("--startIndex", default=0)
@click.option("--sortOrder", default=True)
@click.option("--sortBy", default="name")
def list_epds(pagesize, format, search, startindex, sortorder, sortby):
    """List all available epds.

    For example, dump all epds in a json file: `epdnorway list-epds --pageSize 10 > test.json`

    """
    allepds = list_all(
        pageSize=pagesize,
        format=format,
        search=search,
        startIndex=startindex,
        sortOrder=sortorder,
        sortBy=sortby,
    )
    click.echo(json.dumps(allepds, indent=3))


@cli.command()
@click.option(
    "--dst",
    default="epdnorway.xlsx",
    help="Destination with path",
    type=click.Path(writable=True),
)
@click.option(
    "--pageSize",
    default=None,
    required=False,
    help="How many epds to return. Defaults to 'all'.",
)
def generate_excel(dst, pagesize):
    """Get epds into an excel file."""
    n = pagesize if pagesize else "all"
    click.echo(f"fetching {n} epds...")
    allepds = list_all(pageSize=pagesize)
    df = pd.DataFrame(allepds)

    tqdm.pandas(desc="parsing datasets")
    datasets = df.progress_apply(lambda x: safe_dataset(x.uuid, x.languages), axis=1)

    tqdm.pandas(desc="lcia")
    allLcias = datasets.progress_apply(get_alllcia)
    tqdm.pandas(desc="flows")
    allFlows = datasets.progress_apply(get_allflow_amounts)
    refFlows = datasets.progress_apply(safe_refFlow).apply(
        pd.Series
    )  # .drop(columns=[0])

    refFlows.columns = pd.MultiIndex.from_product(
        [["Reference Flow"], refFlows.columns]
    )

    click.echo("Preparation of the Excel Document...")
    compliance = (
        df.compliance.apply(pd.Series)
        .stack()
        .apply(pd.Series)
        .reset_index()
        .set_index(["level_0", "name"])
        .uuid.unstack("name")
    )
    compliance.name = "compliance"
    compliance.columns = pd.MultiIndex.from_product(
        [["compliance"], compliance.columns.to_list()]
    )

    dataSources = df.dataSources.apply(pd.Series).stack().apply(pd.Series).unstack()
    dataSources.name = "dataSources"
    dataSources.columns = pd.MultiIndex.from_product(
        [["dataSources"], dataSources.columns.to_list()]
    )

    df_mode = df.drop(columns=["compliance", "dataSources"])
    df_mode.columns = pd.MultiIndex.from_product([["data"], df_mode.columns.to_list()])

    big_df = pd.concat(
        [df_mode, compliance, dataSources, refFlows, allFlows, allLcias],
        axis=1,
        verify_integrity=True,
    )

    big_df.to_excel(dst)

    click.echo(f"Excel file created at '{dst}'")


if __name__ == "__main__":
    cli()

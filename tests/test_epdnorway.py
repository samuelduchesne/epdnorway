import pytest

from epdnorway.core import DataSet, list_all, list_query, parse_tag

UUIDs = [epd["uuid"] for epd in list_all(1)]
UUIDs.append("999bb6a9-357c-4377-b416-1b0187ea693d")
UUIDs.append("4a50793b-0e17-4b79-952a-5544195f89e9")
UUIDs.append("c45da52d-e52b-42e3-b462-5c1583641548")
UUIDs.append("347e5992-99ba-4acb-b456-a3c936edb76c")
UUIDs.append("c3bad9a3-f3ce-4841-bd3e-9bc9acd89c0b")


class TestDataSet:
    @pytest.fixture()
    def TestDataSet(self, uuid="308ef5df-28b3-4e7d-a53e-b6b30302b641"):
        """

        Returns:
            DataSet: The DataSet
        """
        yield DataSet(uuid=uuid, lang="en")

    @pytest.mark.parametrize("uuid", UUIDs)
    def test_dataset(self, uuid):
        assert DataSet(uuid)

    def test_available_flows(self, TestDataSet):
        print(TestDataSet)
        assert False

    def test_available_impact_assessments(self):
        assert False

    @pytest.mark.parametrize("flow", ["EEE"])
    def test_get_flow(self, TestDataSet, flow):
        flowObj = TestDataSet.getFlow(flow)
        assert flowObj.acronyme == flow

    @pytest.mark.parametrize("uuid", UUIDs)
    def test_reference_flow(self, uuid):
        TestDataSet = DataSet(uuid=uuid, lang="en")
        refFlow = TestDataSet.referenceFlow
        print(refFlow)
        assert refFlow

    @pytest.mark.parametrize("uuid", UUIDs)
    def test_reference_flow_material_properties(self, uuid):
        TestDataSet = DataSet(uuid=uuid, lang="en")
        refFlow = TestDataSet.referenceFlow
        assert refFlow.material_properties

    def test_get_impact(self):
        assert False

    def test_dump_json(self):
        assert False


@pytest.mark.parametrize("tag", ["u:fuckers", "fuckers"])
def test_parse_tag(tag):
    parsed_tag = parse_tag(tag)
    assert "fuckers" == parsed_tag


def test_list_query():
    data = list_query("limtre")
    assert all("limtre" in epd["name"].lower() for epd in data)

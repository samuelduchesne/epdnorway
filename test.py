#!/usr/bin/python

# Test of API
import urllib
import xml.etree.ElementTree as et

dataStock = "91413340-7bf0-4f88-a952-0f91cba685df"
uuid = "c6990e77-9222-47a4-80a6-dd66edaef1ea"
reqURL = "https://epdnorway.lca-data.com/resource/datastocks/{0}/processes/{1}?format=xml&lang=en".format(
    dataStock, uuid
)
namespaces = {
    "common": "http://lca.jrc.it/ILCD/Common",
    "ns": "http://lca.jrc.it/ILCD/Process",
}


class ParseEpdXmlWithNamespace:
    def __init__(self, url):
        self.tree = et.parse(urllib.urlopen(url))
        self.root = self.tree.getroot()

    def show_root_info(self):
        for item in self.root.items():
            for n, field in enumerate(item):
                if n == 0:
                    print("first field: "), item[n]
                else:
                    print(field)


def tryit():
    epd = ParseEpdXmlWithNamespace(reqURL)
    for child in epd.root:
        print(child.tag, child.attrib)

    for exchanges in epd.root.findall("ns:exchanges", namespaces):
        for _, ex1 in enumerate(exchanges):
            for _, ex2 in enumerate(ex1):
                print(ex2)


if __name__ == "__main__":
    tryit()
